#!/usr/bin/env python3
#Count files in current folder
import os

dirPath = os.path.dirname(os.path.realpath(__file__))
fileCount = os.listdir(dirPath)
print("Objects in this folder:", len(fileCount))
